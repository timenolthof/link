// Copyright © Timen Olthof 2020

use std::fs;
use toml::Value;
use toml::de::Error;
use askama::Template;

// Askama configuration
#[derive(Template)]
#[template(path = "redirect.html")]
struct RedirectTemplate<'a> { 
    target: &'a str,
}

fn main() -> Result<(), Error> {
  // Read config file
  let toml = fs::read_to_string("links.toml").expect("Unable to read file");

  // Parse toml
  let config: Value = toml::from_str(&toml.to_owned())?;
  let links = config["links"].as_table().unwrap();
  println!("Found {:?} links to generate:", links.len());

  // Generate links
  for link in links.iter() {
    println!("-- Generating {:?} - {:?}", link.0, link.1.as_str().unwrap());

    // Generate template
    let redirect_template = RedirectTemplate { target: link.1.as_str().unwrap() };
    let html = redirect_template.render().unwrap();

    // Prepare link directory
    fs::create_dir_all("./public/".to_string() + link.0).expect("Unable to read file");

    // Write template to file
    let html_filename = "./public/".to_string() + link.0 + "/index.html";
    println!("  > Wrote {}", html_filename);
    fs::write(html_filename, html).expect("Unable to write file");

  }    

  println!("Done. Generated {} links.", links.len());
  // TODO: replace unwrap with match, map, or and_then

  Ok(())
}